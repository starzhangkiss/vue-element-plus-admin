import { fileURLToPath, URL } from "node:url";

import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";

import { viteMockServe } from "vite-plugin-mock";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    viteMockServe({
      mockPath: "mock", // mock 文件的路径
      localEnabled: false, // dev 模式是否启用
    }),
  ],
  resolve: {
    alias: {
      "@": fileURLToPath(new URL("./src", import.meta.url)),
    },
  },
  server: {
    proxy: {
      "/user": {
        target: "http://127.0.0.1:8081",
        changeOrigin: true,
        rewrite: (path) => path.replace(/^\/user/, ""),
      },
      "/rbac": {
        target: "http://127.0.0.1:8081",
        changeOrigin: true,
      },
      "/goods": {
        target: "http://127.0.0.1:8082",
        changeOrigin: true,
      },
      "/course": {
        target: "http://127.0.0.1:8084",
        changeOrigin: true,
      },
    },
  },
});
